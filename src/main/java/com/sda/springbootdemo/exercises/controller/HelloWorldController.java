package com.sda.springbootdemo.exercises.controller;

import com.sda.springbootdemo.exercises.model.CounterRequest;
import com.sda.springbootdemo.exercises.model.Person;
import com.sda.springbootdemo.exercises.service.CountingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;

@RestController
//@RequestMapping(path = "/aplikacja/")
public class HelloWorldController {

    @Autowired
    private CountingService countingService;

    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("Hello my new app!");
    }

    @RequestMapping(path = "/counter", method = RequestMethod.GET)
    public ResponseEntity<String> getCounter() {
        String wartoscDozwrocenia = String.valueOf(countingService.getLicznik());

        return ResponseEntity.ok(wartoscDozwrocenia);
    }

    @RequestMapping(path = "/counter", method = RequestMethod.POST)
    public ResponseEntity postCounter() {
        countingService.increment();
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/count", method = RequestMethod.POST)
    public ResponseEntity postCounter(@RequestBody CounterRequest counterRequest) {
        countingService.increment(counterRequest.getHowMany());
        return ResponseEntity.ok().build();
    }

}
